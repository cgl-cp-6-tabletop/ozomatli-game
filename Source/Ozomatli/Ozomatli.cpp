// Fill out your copyright notice in the Description page of Project Settings.

#include "Ozomatli.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Ozomatli, "Ozomatli" );

DEFINE_LOG_CATEGORY( SerialPort );