// Copyright (c) 2017, The Ozomatli Group

#include "SerialPortLineReaderThread.h"
#include <string>
#include <windows.h>

SerialPortLineReaderThread::SerialPortLineReaderThread() :
	_thread( nullptr ),
	_serialPortHandle( nullptr ),
	_doRead( true ),
	_stringQueue( nullptr )
{
}

SerialPortLineReaderThread::~SerialPortLineReaderThread()
{
	delete( _thread );
}

bool SerialPortLineReaderThread::StartReading( void* serialPortHandle, TQueue<FString>& stringQueue )
{
	if ( _thread != nullptr )
	{
		// Thread is already running.
		return false;
	}
	
	_stringQueue = &stringQueue;
	_serialPortHandle = serialPortHandle;
	_thread = FRunnableThread::Create( this, TEXT( "SerialPortLineReader" ), 0, TPri_BelowNormal );
	
	return _thread != nullptr;
}

void SerialPortLineReaderThread::StopReading()
{
	if( _thread == nullptr )
	{
		return;
	}

	_doRead = false;
	_thread->WaitForCompletion();
	delete ( _thread );
	_thread = nullptr;
}

uint32 SerialPortLineReaderThread::Run()
{
	PurgeComm( _serialPortHandle, PURGE_RXABORT | PURGE_TXABORT | PURGE_RXCLEAR | PURGE_TXCLEAR );

	FString currentLine;
	while ( _doRead )
	{
		char buffer = '\0';
		DWORD read;
		if ( ReadFile( _serialPortHandle, &buffer, 1, &read, nullptr ) == FALSE || read == 0 )
		{
			continue;
		}
		
		std::string tempString( 1, buffer );
		currentLine.Append( tempString.c_str() );

		if ( currentLine.Contains( TEXT( "\r\n" ) ) )
		{
			int32 lineEndPosition = currentLine.Find( TEXT( "\r\n" ) );
			FString line = currentLine.Mid( 0, lineEndPosition );
			if( _stringQueue != nullptr )
			{
				_stringQueue->Enqueue( line );
			}
			currentLine.Reset();
		}

		// Sleeping for a millisecond is usually a substantial performance boost for the CPU.
		
	}

	return 0;
}
