// Copyright (c) 2017, The Ozomatli Group

#include "SerialPortConnection.h"
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>
#include <windows.h>

USerialPortConnection* USerialPortConnection::OpenSerialPort( int32 port, int32 baudRate, bool& outOpened )
{
	USerialPortConnection* serialPort = NewObject<USerialPortConnection>();
	outOpened = serialPort->Open( port, baudRate );
	return serialPort;
}

USerialPortConnection::USerialPortConnection() :
	_serialPortHandle( nullptr ),
	_maxTimeBetweenBytes( 0 ),
	_maxReadingTime( 0 ),
	_maxWritingTime( 0 )
{
}

USerialPortConnection::~USerialPortConnection()
{
	Close();
}

bool USerialPortConnection::Open( int32 port, int32 baudRate )
{
	FString portName = FString::Printf( TEXT( "COM%d" ), port );
	return openPort( portName, baudRate );
}

void USerialPortConnection::Close()
{
	_lineReaderThread.StopReading();
	CloseHandle( _serialPortHandle );
	_serialPortHandle = nullptr;
}

bool USerialPortConnection::GetOldestLine( FString& outLine )
{
	return _receivedMessageQueue.Dequeue( outLine );
}

bool USerialPortConnection::openPort( const FString& portName, int32 baudRate )
{
	_serialPortHandle = CreateFile( *portName, GENERIC_READ | GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, 0, nullptr );
	if( _serialPortHandle == INVALID_HANDLE_VALUE )
	{
		CloseHandle( _serialPortHandle );
		return false;
	}

	DCB portDcb = { 0 };
	portDcb.DCBlength = sizeof( portDcb );
	BOOL status = GetCommState( _serialPortHandle, &portDcb );
	if( status == FALSE )
	{
		CloseHandle( _serialPortHandle );
		return false;
	}

	portDcb.BaudRate = baudRate;
	portDcb.ByteSize = 8;
	portDcb.StopBits = ONESTOPBIT;
	portDcb.Parity = NOPARITY;

	status = SetCommState( _serialPortHandle, &portDcb );
	if ( status == FALSE )
	{
		CloseHandle( _serialPortHandle );
		return false;
	}

	_lineReaderThread.StartReading( _serialPortHandle, _receivedMessageQueue );

	OnLineReceived( TEXT( "This is a line!" ) );
	
	return true;
}

void USerialPortConnection::setTimeouts( int32 maxTimeBetweenBytes, int32 maxReadingTime, int32 maxWritingTime )
{
	if( _serialPortHandle == nullptr )
	{
		return;
	}

	_maxTimeBetweenBytes = maxTimeBetweenBytes;
	_maxReadingTime = maxReadingTime;
	_maxWritingTime = maxWritingTime;

	COMMTIMEOUTS timeouts = { 0 };
	timeouts.ReadIntervalTimeout = static_cast<DWORD>( _maxTimeBetweenBytes );
	timeouts.ReadTotalTimeoutConstant = static_cast<DWORD>( _maxReadingTime );
	timeouts.ReadTotalTimeoutMultiplier = 10;
	timeouts.WriteTotalTimeoutConstant = static_cast<DWORD>( _maxWritingTime );
	timeouts.WriteTotalTimeoutMultiplier = 10;

	SetCommTimeouts( _serialPortHandle, &timeouts );
}
