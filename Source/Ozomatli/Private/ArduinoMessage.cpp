// Copyright (c) 2017, The Ozomatli Group

#include "ArduinoMessage.h"

UArduinoMessage* UArduinoMessage::FromString( const FString& sourceString, bool& outSuccessful )
{
	int32 contentOpenBraceIndex;
	if( sourceString.FindChar( '{', contentOpenBraceIndex ) == false )
	{
		outSuccessful = false;
		return nullptr;
	}

	int32 contentCloseBraceIndex;
	if ( sourceString.FindLastChar( '}', contentCloseBraceIndex ) == false )
	{
		outSuccessful = false;
		return nullptr;
	}

	UArduinoMessage* message = NewObject<UArduinoMessage>();

	FString title = sourceString.Mid( 0, contentOpenBraceIndex );
	message->SetMessageTitle( title );

	FString contentString = sourceString.Mid( contentOpenBraceIndex + 1, contentCloseBraceIndex - contentOpenBraceIndex - 1 );
	TArray<FString> contentArrayStrings;
	TArray<int32> contentArrayInts;
	contentString.ParseIntoArray( contentArrayStrings, TEXT( "," ), false );
	for( int i = 0; i < contentArrayStrings.Num(); i++ )
	{
		int32 value = FCString::Atoi( *contentArrayStrings[i] );
		contentArrayInts.Push( value );
	}
	message->SetMessageContent( contentArrayInts );

	outSuccessful = true;
	return message;
}

UArduinoMessage::UArduinoMessage()
{
}

UArduinoMessage::~UArduinoMessage()
{
}

const FString& UArduinoMessage::GetMessageTitle() const
{
	return _messageTitle;
}

void UArduinoMessage::SetMessageTitle( const FString& title )
{
	_messageTitle = title;
}

const TArray<int32>& UArduinoMessage::GetMessageContent() const
{
	return _messageContent;
}

void UArduinoMessage::SetMessageContent( const TArray<int32>& content )
{
	_messageContent = content;
}
