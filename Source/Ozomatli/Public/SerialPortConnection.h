// Copyright (c) 2017, The Ozomatli Group

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "SerialPortLineReaderThread.h"
#include "Queue.h"
#include "SerialPortConnection.generated.h"

/**
 * @brief Handles a serial port (e.g. USB) communication.
 */
UCLASS( BlueprintType, Blueprintable, Category = "Serial Port", meta = ( Keywords = "com arduino serial usb" ) )
class OZOMATLI_API USerialPortConnection : public UObject
{
	GENERATED_BODY()
	
public:
	/**
	 * @brief Creates and opens a serial port.
	 * 
	 * @param port		The com port.
	 * @param baudRate	The port's baud rate.
	 * @param outOpened Whether the port could be opened or not.
	 * 
	 * @returns	A pointer to a USerialPortConnection object.
	 */
	UFUNCTION( BlueprintCallable, meta = ( DisplayName = "Open Serial Port" ), Category = "Serial Port", meta = ( Keywords = "com port serial start" ) )
	static USerialPortConnection* OpenSerialPort( int32 port, int32 baudRate, bool& outOpened );


	/**
	 * @brief Default constructor.
	 */
	USerialPortConnection();

	/**
	 * @brief Destructor.
	 */
	~USerialPortConnection();


	/**
	 * @brief Opens the serial port for communication.
	 * 
	 * @param port		The com port.
	 * @param baudRate	The port's baud rate.
	 * 
	 * @returns True if the connection was opened.
	 */
	UFUNCTION( BlueprintCallable, meta = ( DisplayName = "Open Port" ), Category = "Serial Port", meta = ( Keywords = "com start init" ) )
	bool Open( int32 port, int32 baudRate );

	/**
	 * @brief Closes the serial port.
	 */
	UFUNCTION( BlueprintCallable, meta = ( DisplayName = "Close Port" ), Category = "Serial Port", meta = ( Keywords = "com close" ) )
	void Close();

	UFUNCTION( BlueprintImplementableEvent, meta = ( DisplayName = "Line Receive Event" ), Category = "Serial Port", meta = ( Keywords = "com line receive" ) )
	void OnLineReceived( const FString& line );

	/**
	 * \brief Gets the oldest line in the queue.
	 * 
	 * \param outLine The line that has been received.
	 * 
	 * \returns Whether a line was returned or not.
	 */
	UFUNCTION( BlueprintCallable, meta = ( DisplayName = "Get Oldest Line" ), Category = "Serial Port", meta = ( Keywords = "com read get line" ) )
	bool GetOldestLine( FString& outLine );

	

private:
	bool openPort( const FString& portName, int32 baudRate );
	void setTimeouts( int32 maxTimeBetweenBytes, int32 maxReadingTime, int32 maxWritingTime );

	void* _serialPortHandle;	///< The serial port's handle.
	int32 _maxTimeBetweenBytes;	///< The maximum time in milliseconds between each byte before read() returns.
	int32 _maxReadingTime;		///< The maximum time in milliseconds before read() returns.
	int32 _maxWritingTime;		///< The maximum time in milliseconds before write() returns.

	SerialPortLineReaderThread _lineReaderThread;

	TQueue<FString> _receivedMessageQueue;
};
