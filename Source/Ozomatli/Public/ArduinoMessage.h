// Copyright (c) 2017, The Ozomatli Group

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ArduinoMessage.generated.h"

/**
 * Represents a message that can be send between an Arduino and the game.
 */
UCLASS( Blueprintable, Category = "Arduino", meta = ( Keywords = "arduino message serialisation" ) )
class OZOMATLI_API UArduinoMessage : public UObject
{
	GENERATED_BODY()
	
public:
	/**
	 * @brief Creates an Arduino message from a serialised string.
	 * 
	 * @param sourceString	The serialised string that will be parsed into a message.
	 * @param outSuccessful	True if the message could be serialised.
	 * 
	 * @return The Arduino message object.
	 */
	UFUNCTION( BlueprintCallable, meta = ( DisplayName = "Get Arduino Message From Serialised String" ), Category = "Arduino", meta = ( Keywords = "arduino message serialisation" ) )
	static UArduinoMessage* FromString( const FString& sourceString, bool& outSuccessful );
	
	
	/**
	 * @brief Default constructor.
	 */
	UArduinoMessage();

	/**
	 * @brief Destructor.
	 */
	~UArduinoMessage();


	/**
	 * @brief Gets the message's title. The title preceeds the message content: title{content[0], content[1]};
	 * @returns The title.
	 */
	UFUNCTION( BlueprintCallable, meta = ( DisplayName = "Get Message Title" ), Category = "Arduino", meta = ( Keywords = "arduino message serialisation" ) )
	const FString& GetMessageTitle() const;
	
	/**
	 * @brief Sets the message's title. The title preceeds the message content: title{content[0], content[1]};
	 * @param title The new title.
	 */
	UFUNCTION( BlueprintCallable, meta = ( DisplayName = "Set Message Title" ), Category = "Arduino", meta = ( Keywords = "arduino message serialisation" ) )
	void SetMessageTitle( const FString& title );

	/**
	 * @brief Gets the message's content array: title{content[0], content[1]};
	 * @returns The content array.
	 */
	UFUNCTION( BlueprintCallable, meta = ( DisplayName = "Get Message Content" ), Category = "Arduino", meta = ( Keywords = "arduino message serialisation" ) )
	const TArray<int32>& GetMessageContent() const;

	/**
	 * @brief Sets the message's content array: title{content[0], content[1]};
	 * @param content The content array.
	 */
	UFUNCTION( BlueprintCallable, meta = ( DisplayName = "Set Message Content" ), Category = "Arduino", meta = ( Keywords = "arduino message serialisation" ) )
	void SetMessageContent( const TArray<int32>& content );


private:
	FString			_messageTitle;
	TArray<int32> _messageContent;
};
