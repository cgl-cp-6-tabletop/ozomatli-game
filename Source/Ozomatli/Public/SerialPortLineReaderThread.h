// Copyright (c) 2017, The Ozomatli Group

#pragma once

#include "CoreMinimal.h"
#include "Runnable.h"
#include "RunnableThread.h"
#include "Queue.h"

/**
 * @brief Continuously reads from a given serial port and notifies a callback when it receives a line.
 */
class OZOMATLI_API SerialPortLineReaderThread : public FRunnable
{
public:
	/**
	 * @brief Default constructor.
	 */
	SerialPortLineReaderThread();

	/**
	* @brief Destructor.
	*/
	~SerialPortLineReaderThread();
	
	
	/**
	 * @brief Starts reading from the serial port.
	 * 
	 * @param serialPortHandle	The handle to the serial port.
	 * @param stringQueue		The queue that stores the received messages.
	 * 
	 * @returns True if the thread was created.
	 */
	bool StartReading( void* serialPortHandle, TQueue<FString>& stringQueue );

	/**
	 * @brief Stops the reading thread.
	 */
	void StopReading();

	uint32 Run() override;
	

private:
	FRunnableThread*	_thread;
	void*				_serialPortHandle;
	bool				_doRead;
	
	TQueue<FString>*	_stringQueue;
};
